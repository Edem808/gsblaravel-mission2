<?php 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PdoGsb;
use MyDate;
use App\Post;
use PDF;
class gererVisiteursController extends Controller
{
    function lister()
    {
        $les_visiteurs=PdoGsb::getInfosVisiteurs();
        $nb_visiteurs=PdoGsb::nombredeVisiteurs();
        $view= view('listerVisiteurs')->with('les_visiteurs',$les_visiteurs)
                                      ->with('gestionnaire',session('gestionnaire')
                                      );
                                      
        return $view;
    }
    
    function ajouterSaisie()
    {
        return view('ajouterVisiteur')->with('gestionnaire',session('gestionnaire'));
    }






    function ajouterValider(Request $request)
    {
        $id = $request['id'];
        $nom = $request['nom'];
        $prenom = $request['prenom'];
        $login = PdoGsb::LoginGeneration($nom,$prenom);
        $mdp = PdoGsb::MdpGeneration(5);
        $adresse = $request['adresse'];
        $cp = $request['cp'];
        $ville = $request['ville'];
        $dateEmbauche = $request['dateEmbauche'];
        // affecter $prenom
        PdoGsb::ajouterVisiteur($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche);
        $les_visiteurs=PdoGsb::getInfosVisiteurs();
        $view= view('listerVisiteurs')->with('gestionnaire',session('gestionnaire'))
                                      ->with('les_visiteurs',$les_visiteurs);
        
        return $view;
    }

    function modifierChoix()
    {
        $les_visiteurs=PdoGsb::getInfosVisiteurs();
        $view = view('modifier')->with('gestionnaire',session('gestionnaire'))
                                        ->with('les_visiteurs',$les_visiteurs);
        return $view;
    }

    function modifierSaisie(Request $request)
    {
        $idVisiteur = $request['id'];

        // $nom = $request['nom'];
        // $prenom = $request['prenom'];
        $les_visiteurs=PdoGsb::getInfosVisiteurs();
        $leVisiteur = PdoGsb::getUnVisiteur($idVisiteur);
        $view = view('modifierVisiteursSaisie')->with('gestionnaire',session('gestionnaire'))
                                               ->with('leVisiteur',$leVisiteur)
                                               ->with('les_visiteurs',$les_visiteurs);
        return $view;
    }

    function modifierValider(Request $request)
    {
        $id = $request['id'];
        $nom=$request['nom'];
        $prenom=$request['prenom'];
        $adresse=$request['adresse'];
        $cp=$request['cp'];
        $ville=$request['ville'];
        $dateEmbauche=$request['dateEmbauche'];
        
        $modif = PdoGsb::modifierVisiteur($id, $nom, $prenom, $adresse, $cp, $ville, $dateEmbauche);
        $les_visiteurs= PdoGsb::getInfosVisiteurs();
        $message="le membre a bien été modifié";
        $view = view('listerVisiteurs')->with('gestionnaire',session('gestionnaire'))
                                        ->with('les_visiteurs',$les_visiteurs);
        return $view;
    }

    public function getPostPdf (Post $post)
    {
        dd($post);
        
        $pdf = PDF::loadView('pdf.user');
        $pdf = PDF::loadFile(public_path("documents/fichier.html"));
       $pdf = PDF::loadHTML("<p>Mon contenu HTML ici</p>");
        return $pdf->download('user.pdf');
        
        
    }


}
?>